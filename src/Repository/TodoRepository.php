<?php

namespace Repository;

use Entity\TodoItem;
use DataProvider\TodoDataProvider;

class TodoRepository
{
    /** @var TodoDataProvider */
    private $todoDataProvider;

    /**
     * @param TodoDataProvider $todoDataProvider
     */
    public function __construct(TodoDataProvider $todoDataProvider)
    {
        $this->todoDataProvider = $todoDataProvider;
    }

    /**
     * @return array
     */
    public function findAll()
    {
        return $this->todoDataProvider->findAll();
    }

    /**
     * @param TodoItem $todoItem
     *
     * @return int
     */
    public function addTodo(TodoItem $todoItem)
    {
        $todoItems = $this->todoDataProvider->findAll();

        $todoItem->setId(
            $this->findNextId($todoItems)
        );

        $todoItems[] = $todoItem;

        $this->todoDataProvider->saveItems($todoItems);

        return $todoItem->getId();
    }

    /**
     * @param array $todoItems
     *
     * @return int
     */
    private function findNextId(array $todoItems)
    {
        $highestId = array_reduce($todoItems, function ($previouslyHighest, $item) {
            if ($item->getId() > $previouslyHighest) {
                return $item->getId();
            }

            return $previouslyHighest;
        }, 0);

        return $highestId + 1;
    }
}
