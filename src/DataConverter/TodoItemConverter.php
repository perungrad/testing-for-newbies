<?php

namespace DataConverter;

use Entity\TodoItem;

class TodoItemConverter
{
    /**
     * @param TodoItem $todoItem
     *
     * @return array
     */
    public function convertToArray(TodoItem $todoItem)
    {
        return [
            'id'        => $todoItem->getId(),
            'text'      => $todoItem->getText(),
            'createdAt' => $todoItem->getCreatedAt()->format('Y-m-d H:i:s'),
            'isDone'    => $todoItem->isDone(),
        ];
    }

    /**
     * @param array $itemData
     *
     * @return TodoItem
     */
    public function convertToEntity(array $itemData)
    {
        $todoItem = new TodoItem();

        $todoItem->setId($itemData['id'])
            ->setText($itemData['text'])
            ->setCreatedAt(new \DateTime($itemData['createdAt']))
            ->setIsDone($itemData['isDone']);

        return $todoItem;
    }
}
