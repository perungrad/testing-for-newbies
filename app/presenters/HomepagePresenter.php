<?php

namespace App\Presenters;

use Nette;
use Nette\Application\UI\Form;
use Entity\TodoItem;
use Repository\TodoRepository;

class HomepagePresenter extends Nette\Application\UI\Presenter
{
    /** @var TodoRepository */
    private $todoRepository;

    /**
     * @param TodoRepository $todoRepository
     *
     * @return self
     */
    public function injectTodoRepository(TodoRepository $todoRepository)
    {
        $this->todoRepository = $todoRepository;

        return $this;
    }

    public function renderDefault()
    {
        $this->template->todoItems = $this->todoRepository->findAll();
    }

    protected function createComponentTodoForm()
    {
        $form = new Form();

        $form->addText('text', 'Úloha:');

        $form->onSuccess[] = function (Form $form, \stdClass $values) {
            $todoItem = new TodoItem();
            $todoItem->setText($values->text)
                ->setCreatedAt(new \DateTime())
                ->setAsNotDone();

            $this->todoRepository->addTodo($todoItem);

            $this->redirect('this');
        };

        return $form;
    }
}
